# Configuration of the Docker Machine Driver for *Hetzner Cloud*

The driver for *Hetzner Cloud* was written by Jonas Stoehr who hosts his project
on [Github](https://github.com/JonasProgrammer/docker-machine-driver-hetzner).
Thank you for this awesome piece of work, Jonas!

The environment variables listed below can be used to configure accessing the
Hetzner Cloud API. They are mapped to corresponding command line parameters to
pass them to *Docker Machine* when creating a new machine. Please note that the
default value of environment variables may differ from the original one to
better meet the needs of Gitlab runners.

In the `tools` directory of the repository there is a python script `hetzner-info.py`
that queries the Hetzner Cloud API to retrieve identifiers that can be used to set
the environment variables below.

## >>> HETZNER_API_TOKEN ***[mandatory]***

The token to access the Hetzner Cloud API. The access token can be created via
the [Hetzner Cloud Web Interface](https://www.hetzner.de).

Maps to `--hetzner-api-token` at docker-machine.

## >>> HETZNER_IMAGE

The name of the Hetzner Cloud image to use, see [Images API](https://docs.hetzner.cloud/#images)
for how to get a list.

Maps to `--hetzner-image` at docker-machine.

Available images (retrieved: 2019/07/26):

| **Name**     | Description
| :----------- | :--------------------------------------------------------
| centos-7     | CentOS 7
| debian-9     | Debian 9 (Stretch)
| debian-10    | Debian 10 (Buster)
| fedora-29    | Fedora 29
| fedora-30    | Fedora 30
| ubuntu-16.04 | Ubuntu 16.04 LTS (Xenial Xerus)
| ubuntu-18.04 | Ubuntu 18.04 LTS (Bionic Beaver)

Default: `ubuntu-18.04`

## >>> HETZNER_IMAGE_ID

The id of the Hetzner cloud image (or snapshot) to use, see [Images API](https://docs.hetzner.cloud/#images)
for how to get a list. This mutually excludes HETZNER_IMAGE.

Maps to `--hetzner-image-id` at docker-machine.

Default: *unset*

## >>> HETZNER_EXISTING_KEY_PATH

Use an existing (local) SSH key instead of generating a new keypair.

When you specify `HETZNER_EXISTING_KEY_PATH`, the driver will attempt to copy
*(specified file name)* and *(specified file name).pub* to the machine's store
path. Their public key file's permissions will be set according to your current
umask and the private key file will have 600 permissions.

Please note that the driver will attempt to delete the linked key during machine
removal, unless `HETZNER_EXISTING_KEY_ID` was used during creation.

Maps to `--hetzner-existing-key-path` at docker-machine.

Default: *unset* (generate new keypair)

## >>> HETZNER_EXISTING_KEY_ID

Requires `HETZNER_EXISTING_KEY_PATH` to be set. Use an existing (remote) SSH key
instead of uploading the imported key pair, see [SSH Keys API](https://docs.hetzner.cloud/#ssh-keys)
for how to get a list.

When you specify `HETZNER_EXISTING_KEY_ID` the driver will not create an SSH
key using the API but rather try to use the existing public key corresponding
to the given id. Please note that during machine creation, the driver will
attempt to get the key and compare it's fingerprint to the local public key's
fingerprtint. Keep in mind that both the local and the remote key must be
accessible and have matching fingerprints, otherwise the machine will fail
it's pre-creation checks.

Maps to `--hetzner-existing-key-id` at docker-machine.

Default: `0` (upload new key)

## >>> HETZNER_SERVER_LOCATION

The location to create the server in, see [Locations API](https://docs.hetzner.cloud/#locations)
for how to get a list.

Maps to `--hetzner-server-location` at docker-machine.

Available locations (retrieved: 2019/07/26):

| **Name** | Country | City         | Network Zone | Description
| :------- | :-----: | :----------- | :----------- | :----------------------
| fsn1     | DE      | Falkenstein  | eu-central   | Falkenstein DC Park 1
| nbg1     | DE      | Nuremberg    | eu-central   | Nuremberg DC Park 1
| hel1     | FI      | Helsinki     | eu-central   | Helsinki DC Park 1

Default: *unset* (let Hetzner choose)

## >>> HETZNER_SERVER_TYPE

The type of the Hetzner Cloud server, see [Server Types API](https://docs.hetzner.cloud/#server-types)
for how to get a list.

Maps to `--hetzner-server-type` at docker-machine.

Available server types (retrieved: 2019/07/26):

| **Type**   | Cores  | CPU Type  | Memory (GB) | Disk (GB) | Storage Type
| :--------- | :----: | :-------- | :---------: | :-------: | :-------------
| ccx11      | 2      | dedicated | 8           | 80        | local
| ccx21      | 4      | dedicated | 16          | 160       | local
| ccx31      | 8      | dedicated | 32          | 240       | local
| ccx41      | 16     | dedicated | 64          | 360       | local
| ccx51      | 32     | dedicated | 128         | 540       | local
| cx11       | 1      | shared    | 2           | 20        | local
| cx11-ceph  | 1      | shared    | 2           | 20        | network
| cx21       | 2      | shared    | 4           | 40        | local
| cx21-ceph  | 2      | shared    | 4           | 40        | network
| cx31       | 2      | shared    | 8           | 80        | local
| cx31-ceph  | 2      | shared    | 8           | 80        | network
| cx41       | 4      | shared    | 16          | 160       | local
| cx41-ceph  | 4      | shared    | 16          | 160       | network
| cx51       | 8      | shared    | 32          | 240       | local
| cx51-ceph  | 8      | shared    | 32          | 240       | network

Default: `cx11`

## >>> HETZNER_USER_DATA

Cloud-init based user data.

Maps to `--hetzner-user-data` at docker-machine.

Default: *unset*

## >>> HETZNER_NETWORKS

Comma-separated list of Network IDs or names which should be attached to the
server private network interface.

Maps to `--hetzner-networks` at docker-machine.

Default: *unset*

## >>> HETZNER_VOLUMES

Comma-separated list of Volume IDs or names which should be attached to the server.

Maps to `--hetzner-volumes` at docker-machine.

Default: *unset*
