#!/usr/bin/python3

# This application needs the Hetzner Cloud API library.
# You can install it via pip:
# $ pip3 install hcloud

from hcloud import Client
from hcloud.server_types.domain import ServerType
from hcloud.images.domain import Image

print("This application querys the Hetzner Cloud API to retrieve information about datacenters,")
print("server types and images that can be used. To query the API, your API token is needed.")
api_key = input("Your API token: ")

# create a client
client = Client(token=api_key)

print()
print("Locations:")
print("------------------------------------------------------------------------------")
headers = [ "Id", "Name", "Country", "City", "Network Zone", "Description" ]
table_format="{:<3} {:<10} {:<8} {:<15} {:<15} {}"
print(table_format.format(*headers))
for lc in client.locations.get_all():
  print(table_format.format(lc.id, lc.name, lc.country, lc.city, lc.network_zone, lc.description ))

print()
print("Server Types")
print("------------------------------------------------------------------------------")
headers = [ "Id", "Name", "Cores", "CPU Type", "Memory (GB)", "Disk (GB)", "Storage Type", "Description" ]
table_format="{:<3} {:<10} {:<6} {:<10} {:<12} {:<10} {:<13} {}"
print(table_format.format(*headers))
for st in client.server_types.get_all():
  print(table_format.format(st.id, st.name, st.cores, st.cpu_type, st.memory, st.disk, st.storage_type, st.description ))

print()
print("Images")
print("------------------------------------------------------------------------------")
headers = [ "Id", "Type", "Name", "Image Size (GB)", "Disk Size (GB)", "Rapid-Deploy", "Created", "Description" ]
table_format="{:<8} {:<10} {:<15} {:<16} {:<15} {:<13} {:<26} {}"
print(table_format.format(*headers))
for image in client.images.get_all():
  name = image.name if image.name else "-"
  image_size = "{:.2f}".format(image.image_size) if image.image_size else "-"
  disk_size = "{:.2f}".format(image.disk_size) if image.disk_size else "-"
  print(table_format.format(image.id, image.type, name, image_size, disk_size, image.rapid_deploy, str(image.created), image.description))

print()
print("Networks")
print("------------------------------------------------------------------------------")
headers = [ "Id", "Name", "IP Range" ]
table_format="{:<8} {:<15} {:<15}"
print(table_format.format(*headers))
for nw in client.networks.get_all():
  print(table_format.format(nw.id, nw.name, nw.ip_range))

print()
print("Volumes")
print("------------------------------------------------------------------------------")
headers = [ "Id", "Name", "Size (GB)", "Location", "Created" ]
table_format="{:<8} {:<15} {:<10} {:<20} {:<26}"
print(table_format.format(*headers))
for vol in client.volumes.get_all():
  location = "{} ({})".format(vol.location.city, vol.location.name)
  print(table_format.format(vol.id, vol.name, vol.size, location, str(vol.created)))

