# Auto-Scaling GitLab Runner in the Cloud

[![Pipeline Status](https://gitlab.com/griffinplus/autoscaling-gitlab-runner/badges/master/pipeline.svg)](https://gitlab.com/griffinplus/autoscaling-gitlab-runner/commits/master)

## Overview

This project builds a docker container hosting a *GitLab Runner* that uses
*Docker Machine* to dynamically provision, scale and control VMs at cloud
service providers to run CI jobs. *Docker Machine* comes with built-in support
for the following providers:

- [Amazon Web Services](https://aws.amazon.com)
- [Digital Ocean](https://www.digitalocean.com/)
- [Exoscale](https://www.exoscale.com/)
- [Google Compute Engine](https://cloud.google.com)
- [IBM Softlayer](https://www.softlayer.com)
- [Microsoft Azure](https://azure.microsoft.com)
- [Microsoft Hyper-V](https://docs.microsoft.com/en-us/windows-server/virtualization/virtualization)
- [OpenStack](https://www.openstack.org/)
- [Oracle VirtualBox](https://www.virtualbox.org/)
- [Rackspace](https://www.rackspace.com)
- [VMware Fusion](https://www.vmware.com)
- [VMware vCloud Air](https://www.vmware.com)
- [VMware vSphere](https://www.vmware.com)

Furthermore third-party drivers for *Docker Machine* are included:

- [Hetzner Cloud](https://www.hetzner.de)

The project aims to ease the task of getting an auto-scaling runner off the
ground with very little effort. Sensible default settings were chosen to support
this without taking away the flexibility experienced users need to tweak their
runners.

The [documentation](https://docs.gitlab.com/runner/configuration/autoscale.html)
on auto-scaling Gitlab runners provides additional details on the interior of
auto-scaling runners. Reading the documentation first eases the configuration of
this container.

## Usage

### Step 1) Building the Image

First you need to build the docker image. The `build.sh` script in the repository
helps to generate an appropriate build environment for the tools (*dumb-init*,
*gitlab-runner* and *docker-machine*). According to the environment variables
`DUMB_INIT_VERSION`, `GITLAB_RUNNER_VERSION`, `DOCKER_MACHINE_VERSION` and
`DOCKER_MACHINE_DRIVER_HETZNER_VERSION` the build script fetches the corresponding
source code directly from the official git repositories. If these variables are
not set, the build script querys the repositories to retrieve the latest release
version available. If you just want to get the latest version of all tools, you
don't need to set anything.

```shell
$ DUMB_INIT_VERSION=1.2.2 \
  GITLAB_RUNNER_VERSION=12.1.0 \
  DOCKER_MACHINE_VERSION=0.16.1 \
  DOCKER_MACHINE_DRIVER_HETZNER_VERSION=2.0.1 \
  ./build.sh
```

To build with the latest releases available:

```shell
$ ./build.sh
```

### Step 2) Starting the GitLab Runner

Depending on the cloud service provider you intend to use, different settings
must be specified using environment variables. The examples shown below only set
mandatory settings and use defaults for other settings. It's recommended to
tweak the settings to meet your needs after you see the minimalistic example
working. Almost all settings are accessible via environment variables (see below).

To get the runner off the ground, you only need to specify the following settings:

- `CI_SERVER_URL`: The URL of your GitLab installation (defaults to `https://gitlab.com/`).
- `REGISTRATION_TOKEN`: The registration token that is used to register the
  runner at the specified GitLab instance.
- `MACHINE_DRIVER`: The name of the driver *Docker Machine* uses to access the
  cloud service provider.
- Mandatory settings that are specific to the selected machine driver.

#### Example: Using Digital Ocean

Kicking off an auto-scaling GitLab Runner with default settings that uses
*Digital Ocean* is quite easy. In addition to the settings listed above,
only the access token (`DIGITALOCEAN_ACCESS_TOKEN`) is needed to authenticate
against the Digital Ocean API. The token can be obtained via the Digital Ocean
web interface. Although not strictly required, it's also a good idea to make the
process more verbose at the beginning by setting `LOG_LEVEL` to `debug`. This
can be removed as soon as the runner is working as desired.

```shell
$ docker run --rm \
  -e LOG_LEVEL='debug' \
  -e CI_SERVER_URL='https://gitlab.com/' \
  -e REGISTRATION_TOKEN='YOUR-GITLAB-REGISTRATION-TOKEN' \
  -e MACHINE_DRIVER='digitalocean' \
  -e DIGITALOCEAN_ACCESS_TOKEN='YOUR-DIGITALOCEAN-ACCESS-TOKEN' \
  griffinplus/autoscaling-gitlab-runner
```

## Environment Variables

The following sections describe the environment variables that can be used to
configure *GitLab Runner* and *Docker Machine*. Most of the listed environment
variables are transformed to their corresponding command line parameter
equivalents to improve handling of these arguments and work around known issues
in *GitLab Runner* and *Docker Machine*. Therefore the documentation of these
variables may slightly differ from the original documentation of *GitLab Runner*
and *Docker Machine*. Variables that are not listed are not transformed to
command line parameters and flow through to the *gitlab-runner* and *docker-machine*
processes.

### Runner Registration

The following environment variables influence how the GitLab Runner registers
itself at the specified GitLab instance.

##### >>> REGISTER_ACCESS_LEVEL

The protection level. Can be:

- `not_protected` : Protected and unprotected branches/tags can be built
- `ref_protected` : Only protected branches/tags can be built

Maps to `gitlab-runner register --access-level <value>`.

Default: `not_protected`

##### >>> REGISTER_LOCKED

Determines whether the runner is locked for the registered project. Can be:

- `true`: The runner can be used for the registered project only.
- `false`: The runner can be used for other projects as well.

Maps to `gitlab-runner register --locked=<value>`.

Default: `false`

##### >>> REGISTER_MAXIMUM_TIMEOUT

The maximum job timeout (in seconds). 0 means *unlimited*.

Maps to `gitlab-runner register --maximum-timeout <value>`.

Default: `3600` (1 hour)

##### >>> REGISTER_RUN_UNTAGGED

Determines whether the runner is allowed to run untagged jobs. Can be:

- `true`: The runner is allowed to run untagged jobs.
- `false`: The runner is not allowed to run untagged jobs.

Maps to `gitlab-runner register --run-untagged <value>`.

Default: `true`

##### >>> REGISTRATION_TOKEN ***[ mandatory ]***

The registration token. This token is needed by *GitLab Runner* to register
itself at the specified GitLab instance. It can be obtained at the *CI/CD*
settings page of a GitLab project (for project-scoped runners) or at the *CI/CD*
settings page of a GitLab group (for group-scoped runners).

Maps to `gitlab-runner register --registration-token <value>`.

### Runner Operation

The following environment variables influence the operation of the GitLab Runner.

#### Generic Settings

##### >>> CHECK_INTERVAL

The interval length between new jobs check (in seconds).

Default: `3`

##### >>> CI_SERVER_URL

The URL of the GitLab instance.

Maps to `gitlab-runner register --url <value>`.

Default: `https://gitlab.com/`

##### >>> CLONE_URL

Overwrite the URL for the GitLab instance. Used if the runner can't connect to
GitLab on the URL GitLab exposes itself.

Maps to `gitlab-runner register --clone-url <value>`.

Default: *unset*

##### >>> CONCURRENT

The limit how many jobs globally can be run concurrently.

The most upper limit of jobs using all defined runners.

`0` does **not** mean *unlimited*, it must be greater then `0`.

Default: `1`

##### >>> LOG_LEVEL

The log level (can be `debug`, `info`, `warn`, `error`, `fatal` or `panic`).

Default: `warning`

##### >>> RUNNER_DEBUG_TRACE_DISABLED

Disables the `CI_DEBUG_TRACE` feature. When set to `true`, then debug trace will
remain disabled even if `CI_DEBUG_TRACE` will be set to `true` by the user.

Maps to `gitlab-runner register --debug-trace-disabled=<value>`.

Default: `false`

##### >>> RUNNER_ENV

Comma-separated list of custom environment variables that will be injected into
the build environment, e.g. `key1=value1, key2=value2`.

Maps to `gitlab-runner register --env <key1=value1> --env <key2=value2>`.

Default: *unset*

##### >>> RUNNER_LIMIT

The limit how many jobs can be handled concurrently by this token. 0 simply
means *no limit*, but causes warnings during operation.

Maps to `gitlab-runner register --limit <value>`.

Default: *same as [CONCURRENT](#-concurrent)*

##### >>> RUNNER_NAME

The Runner's description, just informatory.

Maps to `gitlab-runner register --name <value>`.

Default: `autoscale-runner`

##### >>> RUNNER_OUTPUT_LIMIT

The maximum build log size in kilobytes.

Maps to `gitlab-runner register --output-limit <value>`.

Default: `10240` (10 MB)

##### >>> RUNNER_PRE_CLONE_SCRIPT

Commands to be executed on the runner before cloning the Git repository. This
can be used to adjust the Git client configuration first, for example. To insert
multiple commands, separate lines using the "\n" character.

Maps to `gitlab-runner register --pre-clone-script <script>`.

Default: *unset*

##### >>> RUNNER_PRE_BUILD_SCRIPT

Commands to be executed on the runner after cloning the Git repository, but
before executing the build. To insert multiple commands, separate lines using
the "\n" character.

Maps to `gitlab-runner register --pre-build-script <script>`.

Default: *unset*

##### >>> RUNNER_POST_BUILD_SCRIPT

Commands to be executed on the runner just after executing the build, but before
executing the `after_script`. To insert multiple commands, separate lines using
the "\n" character.

Maps to `gitlab-runner register --post-build-script <script>`.

Default: *unset*

##### >>> RUNNER_REQUEST_CONCURRENCY

Maximum number of concurrent requests for new jobs from GitLab. 0 means *no limit*.

Maps to `gitlab-runner register --request-concurrency <value>`.

Default: `0`

##### >>> RUNNER_TAG_LIST

Comma-separated list of tags that are associated with the runner. Tags can be
used to assign certain kinds of jobs to specific runners.

Maps to `gitlab-runner register --tag-list <value>`.

Default: *unset*

#### Docker Settings

##### >>> DOCKER_IMAGE

The image to use in builds.

Maps to `gitlab-runner register --docker-image <value>`.

Default: `ubuntu:18.04`

##### >>> DOCKER_PRIVILEGED

Determines whether the build container is run in *privileged* mode. Usually it
is not recommended, because running a container in privileged mode is a security
risk. In privileged mode the container has full access to the host. Nevertheless
running in *privileged* mode is required, if *docker-in-docker* is required. To
mitigate the induced security risk, build containers are executed in separate
machines by default. You can tune environment variables starting with `MACHINE_`
to circumvent this and allow the runner to reuse machines for multiple jobs.

Maps to `gitlab-runner register --docker-privileged=<value>`.

Default: `true`

#### Machine Settings

##### >>> MACHINE_DRIVER ***[mandatory]***

The *Docker Machine* driver to use. Depending on the selected driver a different
set of additional environment variables is needed to configure the driver (see
*Driver Settings* in the table below). Only few settings cannot be set via
environment variables. These settings can be set using command line parameters
that are passed to *Docker Machine* when it is launched by the GitLab Runner.
The command line parameters can be specified via `MACHINE_OPTIONS` (see below).

Maps to `--machine-machine-driver` at *gitlab-runner*.

| Built-in Driver   | Description              | Driver Settings
| :---------------- | :----------------------- | :-------------------------------------------
| `amazonec2`       | Amazon Web Services      | [Settings](docs/driver-amazonec2.md)
| `azure`           | Microsoft Azure          | [Settings](docs/driver-azure.md)
| `digitalocean`    | Digital Ocean            | [Settings](docs/driver-digitalocean.md)
| `exoscale`        | Exoscale                 | [Settings](docs/driver-exoscale.md)
| `google`          | Google Compute Engine    | [Settings](docs/driver-google.md)
| `hyperv`          | Microsoft Hyper-V        | [Settings](docs/driver-hyperv.md)
| `openstack`       | OpenStack                | [Settings](docs/driver-openstack.md)
| `rackspace`       | Rackspace                | [Settings](docs/driver-rackspace.md)
| `softlayer`       | IBM Softlayer            | [Settings](docs/driver-softlayer.md)
| `virtualbox`      | Oracle VirtualBox        | [Settings](docs/driver-virtualbox.md)
| `vmwarevcloudair` | VMware vCloud Air        | [Settings](docs/driver-vmwarevcloudair.md)
| `vmwarefusion`    | VMware Fusion            | [Settings](docs/driver-vmwarefusion.md)
| `vmwarevsphere`   | VMware vSphere           | [Settings](docs/driver-vmwarevsphere.md)

| Custom Driver     | Description              | Driver Settings
| :---------------- | :----------------------- | :-------------------------------------------
| `hetzner`         | Hetzner Cloud            | [Settings](docs/driver-hetzner.md)

##### >>> MACHINE_IDLE_COUNT

The number of machines, that need to be created and waiting in *Idle* state.

Maps to `gitlab-runner register --machine-idle-nodes <value>`.

Default: `0`

##### >>> MACHINE_IDLE_TIME

The time (in seconds) for a machine to be in *Idle* state before it is removed.

Maps to `gitlab-runner register --machine-idle-time <value>`.

Default: `0`

##### >>> MACHINE_MAX_BUILDS

The number of builds after which a machine is removed.

Maps to `gitlab-runner register --machine-max-builds <value>`.

Default: `1`

##### >>> MACHINE_NAME

The name of the machine. It must contain %s, which will be replaced with a
unique machine identifier.

Maps to `gitlab-runner register --machine-machine-name <value>`.

Default: `auto-scale-%s`

##### >>> MACHINE_OFF_PEAK_IDLE_COUNT

Like `MACHINE_IDLE_COUNT`, but for *Off Peak* time periods.

Maps to `gitlab-runner register --machine-off-peak-idle-count <value>`.

Default: *unset*

##### >>> MACHINE_OFF_PEAK_IDLE_TIME

Like `MACHINE_IDLE_TIME`, but for *Off Peak* time periods.

Maps to `gitlab-runner register --machine-off-peak-idle-time <value>`.

Default: *unset*

##### >>> MACHINE_OFF_PEAK_PERIODS

Time periods when the scheduler is in the *Off Peak* mode. A comma-separated
list of cron-style patterns. A pattern consists of the following fields:

```
[second] [minute] [hour] [day of month] [month] [day of week] [year]
```

Like in the standard cron configuration file, the fields can contain single
values, ranges, lists and asterisks. See [here](https://github.com/gorhill/cronexpr#implementation)
for details.

Maps to `gitlab-runner register --machine-off-peak-periods <value>`.

Example:

```
"* * 0-10,18-23 * * mon-fri *","* * * * * sat,sun *"
=> Monday to Friday, 0:00-10:59 and 18:00-23:59
=> Saturday and Sunday, all the time
```

Default: *unset* (*Off Peak* mode is disabled)

##### >>> MACHINE_OFF_PEAK_TIMEZONE

Time zone for the times given in `MACHINE_OFF_PEAK_PERIODS`. A timezone string
like `Europe/Berlin` (defaults to the locale system setting of the host if
omitted or empty).

Maps to `gitlab-runner register --machine-off-peak-timezone <value>`.

Default: *unset*

##### >>> MACHINE_OPTIONS

Comma-separated list of Docker Machine options. More details can be found in the
[Docker Machine configuration section](https://docs.gitlab.com/runner/configuration/autoscale.html#supported-cloud-providers).

Maps to `gitlab-runner register --machine-machine-options <value>`.

Default: *unset*

#### Cache Settings

The following environment variables configure the connection to cloud storage
that will be used to cache files between jobs. Caches must not be confused with
build artifacts which travel via the coordinator.

##### >>> CACHE_TYPE

Type of the cache. Can be:

- `gcs`: Google Cloud Storage
- `s3`: Amazon S3 (or S3 compatible provider)

Maps to `gitlab-runner register --cache-type <value>`.

Default: *unset*

##### >>> CACHE_PATH

Name of the path to prepend to the cache URL.

Maps to `gitlab-runner register --cache-path <value>`.

Default: *unset* (no prefix)

##### >>> CACHE_SHARED

Enables cache sharing between runners.

Maps to `gitlab-runner register --cache-shared=<value>`.

Default: `false`

##### >>> GOOGLE_APPLICATION_CREDENTIALS ***[mandatory, if CACHE_TYPE = 'gcs']***

Path to the Google JSON key file in the container. Currently only the
*service_account* type is supported. If configured, takes precedence over
`CACHE_GCS_ACCESS_ID` and `CACHE_GCS_PRIVATE_KEY`.

Maps to `gitlab-runner register --cache-gcs-credentials-file <value>`.

##### >>> CACHE_GCS_ACCESS_ID ***[mandatory, if CACHE_TYPE = 'gcs' and GOOGLE_APPLICATION_CREDENTIALS not specified]***

ID of GCP Service Account used to access the storage.

Maps to `gitlab-runner register --cache-gcs-access-id <value>`.

##### >>> CACHE_GCS_BUCKET_NAME ***[mandatory, if CACHE_TYPE = 'gcs']***

Name of the storage bucket where cache will be stored.

Maps to `gitlab-runner register --cache-gcs-bucket-name <value>`.

##### >>> CACHE_GCS_PRIVATE_KEY ***[mandatory, if CACHE_TYPE = 'gcs' and GOOGLE_APPLICATION_CREDENTIALS not specified]***

Private key used to sign GCS requests.

Maps to `gitlab-runner register --cache-gcs-private-key <value>`.

##### >>> CACHE_S3_ACCESS_KEY ***[mandatory, if CACHE_TYPE = 's3']***

The access key specified for your S3 instance.

Maps to `gitlab-runner register --cache-s3-access-key <value>`.

##### >>> CACHE_S3_BUCKET_LOCATION ***[mandatory, if CACHE_TYPE = 's3']***

Name of the S3 region.

Maps to `gitlab-runner register --cache-s3-bucket-location <value>`.

##### >>> CACHE_S3_BUCKET_NAME ***[mandatory, if CACHE_TYPE = 's3']***

Name of the storage bucket where the cache will be stored.

Maps to `gitlab-runner register --cache-s3-bucket-name <value>`.

##### >>> CACHE_S3_INSECURE

Determines whether the S3 service is available via HTTP or HTTPS. Can be:

- `true`: The S3 service is available by HTTP.
- `false`: The S3 service is available by HTTPS.

Maps to `gitlab-runner register --cache-s3-insecure=<value>`.

Default: `false`

##### >>> CACHE_S3_SECRET_KEY ***[mandatory, if CACHE_TYPE = 's3']***

The secret key specified for your S3 instance.

Maps to `gitlab-runner register --cache-s3-secret-key <value>`.

##### >>> CACHE_S3_SERVER

Address (*host*:*port*) of the S3-compatible server.

Maps to `gitlab-runner register --cache-s3-server-address <value>`.

Default: `s3.amazonaws.com`

## Licensing

This project is under the [MIT license](https://gitlab.com/griffinplus/autoscaling-gitlab-runner/blob/master/LICENSE).

The source code does not contain any third-party software, but building the
docker image downloads other software, for example:

- [GitLab Runner](https://gitlab.com/gitlab-org/gitlab-runner)
- [Docker Machine](https://github.com/docker/machine)
- [Docker Machine Driver for Hetzner Cloud](https://github.com/JonasProgrammer/docker-machine-driver-hetzner)
- [dumb-init](https://github.com/Yelp/dumb-init)
- [Alpine Linux Packages for the container filesystem with basic tools](https://pkgs.alpinelinux.org)

The software above is third-party software under their own licenses. It is ok to
build the docker image on your own and use it within your own company, but keep
in mind that distributing the image requires you to collect and pass along the
licenses of each and every software package with the distributed image to comply
with the licenses. This is why there is no prebuilt docker image ready for use.
The administrative overhead is just too big. The images in the [GitLab Container Registry](https://gitlab.com/griffinplus/autoscaling-gitlab-runner/container_registry)
are for development purposes only.